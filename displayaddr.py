#!/usr/bin/env python3

# Display IP address on SSD1306 OLED
#
# Version 2.0
# Copyright (C) 2023 Antony Bayley.  All Rights Reserved.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import polling2
from addressutils import get_hostname
from addressutils import get_ip
from addressutils import ip_address_is_assigned
from framebufferdisplay import FramebufferDisplay

display = FramebufferDisplay('/dev/fb1')

while True:
    hostname = get_hostname()
    ip_address = get_ip()
    display.clear()
    display.text(hostname, 2, 2)
    display.text(ip_address, 2, 2 + display.glyph_height)
    display.render()
    print(f'Hostname: {hostname}, IP address: {ip_address}')

    # If a valid IP address is NOT yet assigned (i.e. loopback or link-local address) poll fast.
    # After a valid private or public IP address has been assigned, poll slowly.
    if ip_address_is_assigned():
        poll_period = 5 * 60  # slow polling period: 5 minutes
    else:
        poll_period = 2       # fast polling period: 2 seconds

    # wait for IP address to change
    polling2.poll(
        lambda: get_ip() != ip_address,
        step=poll_period,
        poll_forever=True
    )
