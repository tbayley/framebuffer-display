# ILI9341 320x240 pixel colour TFT LCD display

![Photo of LCD](./content/ILI9341-lcd-display-320x240.jpg "ILI9341 colour TFT LCD display (320x240 pixels)")

Get a 320x240 pixel ILI9341 display, from suppliers such as:

* [AliExpress](https://www.aliexpress.com/item/1005003936535877.html)
* [eBay](https://www.ebay.co.uk/itm/175095147095)
* [Amazon](https://www.amazon.co.uk/CNBTR-Serial-Module-ILI9341-Support/dp/B01EHH5H3Q/ref=asc_df_B01EHH5H3Q)

Display modules using the ILI9341 display driver chip are available with or
without a resistive touchpanel controller. The example application _weather.py_
uses the touchpanel. However, it will run with reduced functionality on a
non-touchpanel display module.


## Electrical connections

This display module has an SPI interface. The following jumper wires are needed
to connect an ILI9341 display module to the 40-pin Raspberry Pi GPIO connector.

| ILI9341 LCD Pin Number | Raspberry Pi GPIO Pin Number   | Description                                            |
| :--------------------: | :----------------------------: | :----------------------------------------------------: |
|      1 (VCC)           |          2 (5V)                | 5V / 3.3V power input                                  |
|      2 (GND)           |          9 (GND)               | Ground                                                 |
|      3 (CS)            |         24 (SPI0_CE0_N)        | LCD SPI chip select, low level enable                  |
|      4 (RESET)         |         16 (GPIO23)            | LCD reset, low level enable                            |
|      5 (DC)            |         18 (GPIO24)            | LCD register select (high), data select (low)          |
|      6 (SDI / MOSI)    |         19 (SPI0_MOSI)         | LCD SPI data in                                        |
|      7 (SCK)           |         23 (SPI0_CLK)          | LCD SPI clock                                          |
|      8 (LED)           |         12 (GPIO18 / PWM0)     | LCD backlight control (if not used, connect to 3.3V)   |
|      9 (SDO / MISO)    |         21 (SPI0_MISO)         | LCD SPI data out (leave disconnected if not required)  |
|     10 (T_CLK)         |         23 (SPI0_CLK)          | Touchpanel SPI clock                                   |
|     11 (T_CS)          |         26 (SPI_CE1_N)         | Touchpanel SPI chip select, low level                  |
|     12 (T_DIN)         |         19 (SPI0_MOSI)         | Touchpanel SPI data in (MOSI)                          |
|     13 (T_DO)          |         21 (SPI0_MISO)         | Touchpanel SPI data out (MISO)                         |
|     14 (T_IRQ)         |         22 (GPIO25)            | Touchpanel interrupt, low level when touch is detected |

Some of the Raspberry Pi's SPI port pins are connected to more than one pin on
the display module because the display driver and the touchpanel controller are
separate devices, each with their own SPI interface.


## Configure Device Tree Overlays (Linux display driver) ###

This software uses a Linux kernel driver for the display. The driver is loaded
at boot-time by configuring device-tree overlays. To load the device tree
overlays for an ILI9341 320x240 pixel colour TFT LCD display, and configure the
Raspberry Pi's SPI interface, it is necessary to edit the file _/boot/config.txt_
and include the _watterott-display_ device tree overlay. Framebuffer _/dev/fb0_
is used for the main system display on the Raspberry Pi's HDMI connector.
Framebuffer _/dev/fb1_ is used for the LCD display module.

By default, if the HDMI cable is not connected to a monitor, the Linux system
uses framebuffer _/dev/fb0_ as the data source for the first display that is
detected. It makes no sense to use a small LCD as the main system display, so
we must override this behaviour. At the time of writing (December 2022), the
current version of Raspberry Pi OS is _"Bullseye"_. The default video driver for
_"Bullseye"_ is DRM VC4 V3D, but _"Buster"_ and earlier OS versions use a fake
KMS video driver. These two video drivers have different ways to to force an
association between the HDMI video interface and default framebuffer _/device/fb0_
when the HDMI cable is disconnected. Therefore, different device tree overlays
are required for _Bullseye_ and earlier Raspberry Pi OS versions.


### Raspberry Pi OS version Bullseye or later

If you are using Raspberry Pi OS _"Bullseye"_ or later, the default video driver
is DRM VC4 V3D, which does not support the option _hdmi_force_hotplug=1_ to
force the HDMI interface to be associated with the default framebuffer _/dev/fb0_.
The solution is to revert to the old fake kms driver, and set _max\_framebuffers=3_.

```bash
# Configure ILI9341 320x240 pixel LCD display with touchscreen, using SPI interface
dtoverlay=watterott-display,rotate=90
dtparam=spi=on

# Configure  HDMI display to use /dev/fb0, even if HDMI cable is not plugged
# in, so that the OLED display always uses /dev/fb1 and does not become the
# main system console.
hdmi_force_hotplug=1

# Disable the default DRM VC4 V3D video driver
#dtoverlay=vc4-kms-v3d

# Use the old fake kms video driver that supports hdmi_force_hotplug=1 option
dtoverlay=vc4-fkms-v3d
max_framebuffers=3
```


### Raspberry Pi OS version Buster or earlier

```bash
# Configure ILI9341 320x240 pixel LCD display with touchscreen, using SPI interface
dtoverlay=watterott-display,rotate=90
dtparam=spi=on

# Configure  HDMI display to use /dev/fb0, even if HDMI cable is not plugged
# in, so that the OLED display always uses /dev/fb1 and does not become the
# main system console.
hdmi_force_hotplug=1
```

The _watterott-display_ device tree overlay replaces a deprecated overlay
_rpi-display_. If you are using an older version of Raspberry Pi OS that does
not support the _watterott-display_ device tree overlay, try _rpi-display_
instead.


## Verifying that display module is succesfully connected and configured

After rebooting the Raspberry Pi, use the _lsmod_ command to verify that the
relevant kernel modules have been loaded:

```bash
pi@raspi1:~ $ lsmod | grep fbtft
fbtft                  45056  1 fb_ili9341
syscopyarea            16384  2 fbtft,drm_kms_helper
sysfillrect            16384  2 fbtft,drm_kms_helper
sysimgblt              16384  2 fbtft,drm_kms_helper
fb_sys_fops            16384  2 fbtft,drm_kms_helper
backlight              20480  3 fbtft,drm_kms_helper,drm
```

When the _watterott-display_ device tree overlay is included, the Raspberry Pi
implements a second Linux framebuffer device _/dev/fb1_. To check that the
display is wired up and configured correctly, you can test it from the command
line with the following _bash_ commands:

```bash
cat /dev/urandom > /dev/fb1    # write random data to the pixels
cat /dev/zero > /dev/fb1       # clear the display (all pixels off)
cat /dev/zero | tr "\000" "\377" > /dev/fb1    # set all pixels on
```

If you want to find out more about Linux kernel modules, perhaps to adapt the
software in this repository to work with a different framebuffer-based display
module, the following references may be useful. All available Raspberry Pi kernel
modules, and a brief description of how they are used, are listed here:

* https://github.com/raspberrypi/firmware/blob/master/boot/overlays/README

The following document explains in greater detail how to use device trees on Raspberry Pi:

* https://www.raspberrypi.org/documentation/configuration/device-tree.md#part3


## Verifying that the touchpanel is succesfully connected and configured

The LCD panel uses XPT2046 (compatible with ADS7846) chip as the controller for
the resistive touchscreen. The corresponding kernel module is ads7846. It is
included as a sub-component of the _watterott-display_ device-tree overlay, so
a separate dtoverlay line in _boot/config.txt_ is not required.

After rebooting the Raspberry Pi, use the _lsmod_ command to verify that the
ADS7846 kernel modules have been loaded:

```bash
pi@raspi1:~/framebuffer-display $ lsmod | grep ads7846
ads7846                20480  0
```

When properly configured, the touchpanel should create a HID event device at
_/dev/input/event0_. However, if other input devices such as a mouse or keyboard
are attached to the Raspberry Pi, the touchpanel may be mapped to a different
event such as _/dev/input/event1_, _/dev/input/event3_, etc.

The easiest way to test the touchpanel is to use the utility _evtest_, which
shows the input device name and its decoded touchpanel events. This utility can
be installed with the command `sudo apt install evtest`.

Assuming that the touchpanel is mapped to _/dev/input/event0_, the command
`evtest /dev/input/event0` is used to view touch coordinates and touch pressure.

```bash
pi@raspi1:~/framebuffer-display $ evtest /dev/input/event0
Input driver version is 1.0.1
Input device ID: bus 0x0 vendor 0x0 product 0x0 version 0x0
Input device name: "ADS7846 Touchscreen"
Supported events:
  Event type 0 (EV_SYN)
  Event type 1 (EV_KEY)
    Event code 330 (BTN_TOUCH)
  Event type 3 (EV_ABS)
    Event code 0 (ABS_X)
      Value      0
      Min        0
      Max     4095
    Event code 1 (ABS_Y)
      Value      0
      Min        0
      Max     4095
    Event code 24 (ABS_PRESSURE)
      Value      0
      Min        0
      Max      255
Properties:
Testing ... (interrupt to exit)
Event: time 1672260191.017319, type 1 (EV_KEY), code 330 (BTN_TOUCH), value 1
Event: time 1672260191.017319, type 3 (EV_ABS), code 0 (ABS_X), value 1872
Event: time 1672260191.017319, type 3 (EV_ABS), code 1 (ABS_Y), value 2439
Event: time 1672260191.017319, type 3 (EV_ABS), code 24 (ABS_PRESSURE), value 169
Event: time 1672260191.017319, -------------- SYN_REPORT ------------
Event: time 1672260191.037334, type 3 (EV_ABS), code 0 (ABS_X), value 1864
Event: time 1672260191.037334, type 3 (EV_ABS), code 1 (ABS_Y), value 2448
Event: time 1672260191.037334, type 3 (EV_ABS), code 24 (ABS_PRESSURE), value 177
...
```

Type Ctrl+C to exit.
