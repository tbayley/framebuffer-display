#!/usr/bin/env python3

# Display album cover image on ILI9341 TFT LCD.
#
# Version 1.0
# Copyright (C) 2023 Antony Bayley.  All Rights Reserved.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from framebufferdisplay import FramebufferDisplay

RED = 0xF800 # colour red as 16-bit RGB565 value
GREEN = 0x07E0 # colour green as 16-bit RGB565 value
displayfont = FramebufferDisplay.FONT_VGA_9x16
display = FramebufferDisplay('/dev/fb1', displayfont)
xmax = display.xsize - 1  # maximum X-axis coordinate value
ymax = display.ysize - 1  # maximum Y-axis coordinate value
display.rect(*(0, 0), *(xmax, ymax), GREEN)  # green border
display.rect(*(1, 1), *(xmax - 1, ymax - 1), GREEN)
display.text('Redemption Song [Band Version]', *(25, 215), RED)
display.image('content/uprising-album-cover-500x495.jpg', *(60, 10), *(259, 209))
display.render()
