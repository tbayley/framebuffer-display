#!/usr/bin/env python3

# Address utilities for Linux
#
# Version 1.0
# Copyright (C) 2020 Antony Bayley.  All Rights Reserved.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import socket
import bluetooth  # requires pybluez package


def get_ip() -> str:
    """
    Get the primary IP address of the host.
    :return: IP address.
    """
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    try:
        # this is a private address that does not have to be routable
        s.connect(('10.255.255.255', 1))
        ip = s.getsockname()[0]
    except Exception:
        ip = '127.0.0.1'
    finally:
        s.close()
    return ip


def ip_address_is_assigned() -> bool:
    """
    Test whether a valid IP address has been assigned (i.e. not a loopback or link-local address).
    :return: True if IP address is valid, else False.
    """
    valid = True
    ip_address = get_ip()
    if ip_address.startswith('127.'):
        valid = False  # loopback address
    if ip_address.startswith('169.254.'):
        valid = False  # link-local address
    return valid


def get_hostname() -> str:
    """
    Get the hostname.
    :return: Hostname.
    """
    return socket.gethostname()


def get_bdaddr() -> str:
    """
    Get the Bluetooth MAC address of this host.
    :return: Bluetooth MAC address.
    """
    try:
        bdaddr = bluetooth.read_local_bdaddr()[0]
    except bluetooth.btcommon.BluetoothError:

        bdaddr = 'No Bluetooth'
    return bdaddr


if __name__ == '__main__':
    print(get_ip())
    print(get_bdaddr())
