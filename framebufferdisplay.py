#!/usr/bin/env python3

# Python utilities for framebuffer based OLED / LCD displays on Linux.
#
# Version 2 of framebufferdisplay.py adds support for colour displays.
#
# Version 2.0
# Copyright (C) 2023 Antony Bayley.  All Rights Reserved.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import struct
from freetype import *
from framebufferscreeninfo import FramebufferScreenInfo
from enum import Enum

try:
    import fcntl
except ImportError:
    # mock fcntl module enables limited testing on Windows.
    fcntl = None


def abspath(relativepath: str) -> str:
    """
    Convert relative file path to absolute file path.

    :param relativepath: file path relative to this script.
    :return: absolute file path.
    """
    directory = os.path.dirname(__file__)
    absolutepath = os.path.join(directory, relativepath)
    return absolutepath


def get_bitvalue(value: int, bit: int) -> int:
    """
    Return the specified bit value (0 or 1).

    :param value: Integer value.
    :param bit: The bit to be tested. 0 is the least significant bit.
    :return: The bit value: 0 or 1.
    """
    result = (value >> bit) & 1
    return result


def set_bitvalue(intobj: int, bit: int, bitvalue: int):
    """
    Set the specified bit of an integer object to the bitvalue provided (0 or 1).

    :param intobj: Integer object whose bit is to be set.
    :param bit: The bit to be set. 0 is the least significant bit.
    :param bitvalue: The bit value: 0 or 1.
    :return: A copy of the integer object with the specified bit value set.
    """
    if 0 == bitvalue:
        # clear the specified bit
        bitmask = ~(1 << bit)
        intobj &= bitmask
    else:
        # set the specified bit
        bitmask = 1 << bit
        intobj |= bitmask
    return intobj


class Framebuffer:
    """
    Graphical display primitives for displays that use Linux framebuffer.

    The write() method enables an individual display pixel to be set or erased.
    The pixel position is specified by its (X, Y) coordinates. The pixel colour
    and brightness are set by a value of length self._bits_per_pixel bits.
    For colour displays, the value is composed of Red, Green and Blue components,
    of lengths self._red_bits, self._green_bits and self._blue_bits respectively.

    The image drawing methods act on a local copy of the frame that is stored in
    a Python bytearray. Rendering the image to the display is achieved simply by
    copying the bytes from the bytearray to the associated Linux framebuffer
    device. The top-left display pixel, with XY coordinates (0, 0) corresponds
    to the least-significant self._bits_per_pixel bits in the framebuffer. The
    next pixel on the top line of the display, with coordinates (1, 0),
    corresponds to the next self._bits_per_pixel bits, and so on.

    The Framebuffer class was originally developed for use with 128x32 and
    128x64 pixel monochrome OLED displays using SSD1306 display driver and I2C
    interface. A Linux kernel-mode driver for these displays is available by
    including the ssd1306 device-tree overlay in /boot/config.txt. For example:

        dtoverlay=ssd1306,inverted,sequential,width=128,height=32
        dtparam=i2c_arm=on
        dtparam=i2c_arm_baudrate=400000

    The SSD1306 display driver instantiates a Linux framebuffer /dev/fb1.

    Subsequently, the Framebuffer class was extended for use with 320x240 pixel
    colour TFT LCD displays using teh ILI9341 display driver and SPI interface.
    A Linux kernel-mode driver for these displays is available by including the
    watterott-display device-tree overlay in /boot/config.txt. For example:

        dtoverlay=watterott-display,rotate=90
        dtparam=spi=on

    The Framebuffer class should also work for other displays whose drivers use
    a Linux framebuffer. 

    If xsize and ysize parameters are not specified, their values will be read
    from the properties of the specified Linux framebuffer using ioctl.

    If the framebuffer parameter is set to None, then the Framebuffer class'
    render method will raise an exception. However, the plot and __repr__
    methods work can still be used to visualise the Framebuffer's display data.
    This is useful for testing embedded display driver code on a laptop or
    desktop system that is not connected to the real display hardware.
    """

    class ColourMode(Enum):
        """Class to describe the display's colour mode."""
        BW = 0         # Black and white (bits_per_pixel = 1)
        GRAYSCALE = 1  # Grayscale
        RGB = 2        # Colour, with fields in the order Red, Green, Blue (MSB to LSB)
        BGR = 3        # Colour, with fields in the order Blue, Green, Red (MSB to LSB)


    def __init__(self, framebuffer: str):
        """
        Framebuffer object initialiser.

        :param framebuffer: Filesystem path of Linux framebuffer, e.g. '/dev/fb1'.
                            Alternatively, you can use one of the following strings to implement a simulated display:

                            'bw': Simulation of a 128 x 32 pixel black and white display
                            'grayscale': Simulation of a 128 x 64 pixel 4-bit grayscale display
                            'colour': Simulation of a 320 x 240 pixel 24-bit colour display (RGB888)

                            Simulated displays are useful for developing software on a machine that does not have a
                            real framebuffer display attached. The render() method is not supported for simulated
                            displays. Instead, use the plot() method to render the framebuffer contents in a matplotlib
                            image window.
        """
        self._framebuffer = framebuffer

        if framebuffer == 'bw':
            # Simulation of a 128 x 32 pixel black and white display
            self._xsize = 128
            self._ysize = 32
            self._bits_per_pixel = 1
            self._red_bits = 0
            self._green_bits = 0
            self._blue_bits = 0
            self._red_offset = 0
            self._green_offset = 0
            self._blue_offset = 0
        elif framebuffer == 'grayscale':
            # Simulation of a 128 x 64 pixel 4-bit grayscale display
            self._xsize = 128
            self._ysize = 64
            self._bits_per_pixel = 4
            self._red_bits = 0
            self._green_bits = 0
            self._blue_bits = 0
            self._red_offset = 0
            self._green_offset = 0
            self._blue_offset = 0
        elif framebuffer == 'colour':
            # Simulation of a 320 x 240 pixel 24-bit colour display (RGB888)
            self._xsize = 320
            self._ysize = 240
            self._bits_per_pixel = 24
            self._red_bits = 8
            self._green_bits = 8
            self._blue_bits = 8
            self._red_offset = 16
            self._green_offset = 8
            self._blue_offset = 0
        else:
            # read display information using FBIOGET_VSCREENINFO ioctl.
            screeninfo = FramebufferScreenInfo(framebuffer)
            self._xsize = screeninfo.xres
            self._ysize = screeninfo.yres
            self._bits_per_pixel = screeninfo.bits_per_pixel
            self._red_bits = screeninfo.red.length
            self._green_bits = screeninfo.green.length
            self._blue_bits = screeninfo.blue.length
            self._red_offset = screeninfo.red.offset
            self._green_offset = screeninfo.green.offset
            self._blue_offset = screeninfo.blue.offset

        if (self._xsize * self._bits_per_pixel) % 8 != 0:
            raise ValueError("xsize * bits_per_pixel must be an exact multiple of 8")

        if (self._bits_per_pixel > 8) and (0 != self._bits_per_pixel % 8):
            raise ValueError("bits_per_pixel must either be 1, 2, 4 or an exact multiple of 8")
        
        if (self._bits_per_pixel < 8) and (0 != 8 % self._bits_per_pixel):
            raise ValueError("bits_per_pixel must either be 1, 2, 4 or an exact multiple of 8")
        
        if self._bits_per_pixel == 1:
            self._colour_mode = self.ColourMode.BW
        elif self._red_bits == 0 and self._green_bits == 0 and self._blue_bits == 0:
            self._colour_mode = self.ColourMode.GRAYSCALE
        elif self._red_offset > self._green_offset and self._green_offset > self._blue_offset:
            self._colour_mode = self.ColourMode.RGB
        elif self._blue_offset > self._green_offset and self._green_offset > self._red_offset:
            self._colour_mode = self.ColourMode.BGR
        else:
            raise RuntimeError('Unsupporteed colour mode')

        self._bytes_per_pixel = self._bits_per_pixel // 8
        self._bitmap = bytearray((self._xsize * self._ysize * self._bits_per_pixel) // 8)
        self._white_value = pow(2, self._bits_per_pixel) - 1  # white value: all bits set to 1
        self._black_value = 0  # black value: all bits set to 0
        self._plot_dependencies_imported = False
        self._image_dependencies_imported = False
    
    @property
    def framebuffer(self) -> str:
        """Filesystem path of Linux framebuffer."""
        return self._framebuffer

    @property
    def xsize(self) -> int:
        """Display size in the X direction (width)."""
        return self._xsize

    @property
    def ysize(self) -> int:
        """Display size in the Y direction (height)."""
        return self._ysize

    @property
    def bits_per_pixel(self) -> int:
        """Bits per pixel."""
        return self._bits_per_pixel

    @property
    def red_bits(self) -> int:
        """Red bits per pixel."""
        return self._red_bits

    @property
    def green_bits(self) -> int:
        """Green bits per pixel."""
        return self._green_bits

    @property
    def blue_bits(self) -> int:
        """Blue bits per pixel."""
        return self._blue_bits

    @property
    def colour_mode(self) -> ColourMode:
        """Colour mode."""
        return self._colour_mode

    def write(self, x: int, y: int, value: int):
        """
        Write the pixel value at coordinates (x,y).

        For colour displays, the value is commonly a struct containing Red,
        Green and Blue fields of size self._red_bits, self._green_bits and
        self._blue_bits respectively.
        
        For monochrome displays, the value is commonly a single bit: 0 or 1.

        However, there are a multitude of different displays with different
        pixel data formats. It is left for the user to provide a value parameter
        that matches the pixel data schema of the display being used.

        :param x: X coordinate of the pixel.
        :param y: Y coordinate of the pixel.
        :param value: Pixel value, as an integer of length self._bits_per_pixel.
        """
        if self._bits_per_pixel < 8:
            self._write_bits(x, y, value)
        else:
            self._write_bytes(x, y, value)
        
    def _write_bits(self, x: int, y: int, value: int):
        """
        Write pixel value one bit at a time.

        :param x: X coordinate of the pixel.
        :param y: Y coordinate of the pixel.
        :param value: Pixel value, as an integer of length self._bits_per_pixel.
        """
        bitindex = ((self._xsize * y) + x) * self._bits_per_pixel
        byteindex = (bitindex) // 8
        startbitindex = (bitindex) % 8
        for i in range(self._bits_per_pixel):
            bitvalue = get_bitvalue(value, i)
            self._bitmap[byteindex] = set_bitvalue(self._bitmap[byteindex], startbitindex + i, bitvalue)

    def _write_bytes(self, x: int, y: int, value: int):
        """
        Write pixel value one byte at a time.

        :param x: X coordinate of the pixel.
        :param y: Y coordinate of the pixel.
        :param value: Pixel value, as an integer of length self._bits_per_pixel.
        """
        startbyteindex = ((self._xsize * y) + x) * self._bytes_per_pixel
        for i in range(self._bytes_per_pixel):
            bytevalue = (value >> (8 * i)) & 0xFF
            byteindex = startbyteindex + i
            self._bitmap[byteindex] = bytevalue

    def clear(self):
        """Clear the entire display (unset all pixels)."""
        self._bitmap = bytearray((self._xsize * self._ysize * self ._bits_per_pixel) // 8)

    def read(self, x: int, y: int) -> int:
        """
        Read the pixel value at coordinates (x,y).

        :param x: X coordinate of the pixel to be read.
        :param y: Y coordinate of the pixel to be read.
        :return: The pixel value at coordinates (x,y), which is an integer of length self._bits_per_pixel.
        """
        if self._bits_per_pixel < 8:
            return self._read_bits(x, y)
        else:
            return self._read_bytes(x, y)

    def _read_bits(self, x: int, y: int) -> int:
        """
        Read the pixel value at coordinates (x,y) one bit at a time.

        :param x: X coordinate of the pixel to be read.
        :param y: Y coordinate of the pixel to be read.
        :return: The pixel value at coordinates (x,y), which is an integer of length self._bits_per_pixel.
        """
        bitindex = ((self._xsize * y) + x) * self._bits_per_pixel
        byteindex = (bitindex) // 8
        startbitindex = (bitindex) % 8
        value = 0
        for i in range(self._bits_per_pixel)[::-1]:
            value = (value << 1) | get_bitvalue(self._bitmap[byteindex], startbitindex + i)
        return value

    def _read_bytes(self, x: int, y: int) -> int:
        """
        Read the pixel value at coordinates (x,y) one bit at a time.

        :param x: X coordinate of the pixel to be read.
        :param y: Y coordinate of the pixel to be read.
        :return: The pixel value at coordinates (x,y), which is an integer of length self._bits_per_pixel.
        """
        startbyteindex = ((self._xsize * y) + x) * self._bytes_per_pixel
        value = 0
        for i in range(self._bytes_per_pixel):
            byteindex = startbyteindex + i
            value = value | self._bitmap[byteindex]  << (8 * i)
        return value       

    def render(self):
        """Render the image to the display, by writing to Linux framebuffer."""
        with open(self._framebuffer, 'wb') as fb:
            fb.write(self._bitmap)

    def plot(self):
        """
        Render the image as a Matplotlib plot.

        The initial implementation is a monochrome black and white display. It
        will later be extended to a colour display.
        """
        if not self._plot_dependencies_imported:
            # numpy and matplotlib are conditional imports, which are only imported if the plot() method is called.
            # This significantly speeds up program execution for applications that do not use the plot() function.
            import numpy
            import matplotlib.pyplot as plt
            self._plot_dependencies_imported = True

        if self._colour_mode == self.ColourMode.BW:
            pixel_formatter_func = lambda value: value
        elif self._colour_mode == self.ColourMode.GRAYSCALE:
            divider = pow(2, self._bits_per_pixel) - 1
            pixel_formatter_func = lambda value: value / divider
        else:  #RGB or BGR
            bit_shifts = (self._red_offset, self._green_offset, self._blue_offset)
            bit_masks = (pow(2, self._red_bits) - 1, pow(2, self._green_bits) -1, pow(2, self._blue_bits) - 1)
            dividers = bit_masks
            pixel_formatter_func = lambda value: tuple((((value >> bit_shift) & bit_mask) / divider for bit_shift, bit_mask, divider in zip(bit_shifts, bit_masks, dividers)))

        data = []
        for y in range(self._ysize):
            for x in range(self._xsize):
                # expand the packed bitmap data to a python list of 0 and 1 values
                value = self.read(x, y)
                formatted_value = pixel_formatter_func(value)
                data.append(formatted_value)
        if self._colour_mode == self.ColourMode.BW or self._colour_mode == self.ColourMode.GRAYSCALE:
            plotdata = numpy.array(data).reshape(self._ysize, self._xsize)
            plt.imshow(plotdata, interpolation='nearest', cmap=plt.cm.gray, origin='upper')
        else:
            plotdata = numpy.array(data).reshape(self._ysize, self._xsize, 3)
            plt.imshow(plotdata, origin='upper')
        plt.gca().set_aspect('equal', adjustable='box')
        plt.show()

    def _colour_threshold_func(self, value: int) -> bool:
        """
        Threshold function for colur pixel values.
        
        :return: True if pixel value is greater than half full-scale luminosity.
                 False if pixel value is less than half full-scale luminosity.
        """
        bit_shifts = (self._red_offset, self._green_offset, self._blue_offset)
        bit_masks = (pow(2, self._red_bits) - 1, pow(2, self._green_bits) -1, pow(2, self._blue_bits) - 1)
        dividers = bit_masks
        rgb_float = tuple(((value >> bit_shift) & bit_mask) / divider for bit_shift, bit_mask, divider in zip(bit_shifts, bit_masks, dividers))
        luminosity = 0.3 * rgb_float[0] + 0.59 * rgb_float[1] + 0.11 * rgb_float[2]
        return (luminosity >= 0.5)

    def __repr__(self) -> str:
        """
        Render the image for viewing on a text console.

        If fb is an object of type Framebuffer, its image is rendered to the
        text console by executing the command:

            print(fb)

        The character '#' is printed for each pixel whose luminosity is greater
        than half of the full-scale range, and the character '.' for pixels with
        luminosity less than half of full-scale.

        :return: String representation of the framebuffer image.
        """
        if self._colour_mode == self.ColourMode.BW:
            pixel_threshold_func = lambda value: value == 1
        elif self._colour_mode == self.ColourMode.GRAYSCALE:
            threshold = pow(2, self._bits_per_pixel) // 2
            pixel_threshold_func = lambda value: value >= threshold
        else:  #RGB or BGR
            pixel_threshold_func = self._colour_threshold_func
        imagestring = ''
        for y in range(self._ysize):
            for x in range(self._xsize):
                imagestring += '#' if pixel_threshold_func(self.read(x, y)) else '.'
            imagestring += '\n'
        return imagestring


class FramebufferDisplay(Framebuffer):
    """
    Graphical display utilities for monochrome displays using Linux framebuffer.

    FramebufferDisplay is a subclass of Framebuffer that implements additional
    methods to draw geometric shapes and write text to a graphical display.
    """

    FONT_VGA_9x16 = 'fonts/oldschool/BmPlus_IBM_VGA_9x16.otb'  # monospace: 9x16 pixels
    FONT_EGA_8x14 = 'fonts/oldschool/BmPlus_IBM_EGA_8x14.otb'  # monospace: 8x14 pixels
    FONT_PROGGY_CLEAN_7x13 = 'fonts/proggyfonts/ProggyClean_7x13.pcf.gz'  # monospace: 7x13 pixels
    FONT_PROGGY_SMALL_7x10 = 'fonts/proggyfonts/ProggySmall_7x10.pcf.gz'  # monospace: 7x10 pixels

    def __init__(self, framebuffer: str, font: str = 'default'):
        super().__init__(framebuffer)
        # Configure the font for text rendering
        self._proggyfont = False
        self._rightpadding = 0
        if FramebufferDisplay.FONT_PROGGY_SMALL_7x10 == font:
            self._font = abspath(font)
            self._face = Face(self._font)
            self._glyph_width = 7  # width of text glyph bitmaps
            self._glyph_height = 10  # height of text glyph bitmaps
            self._glyph_xspace = 1  # horizontal space between adjacent glyphs
            self._proggyfont = True
            self._rightpadding = 1
        elif FramebufferDisplay.FONT_PROGGY_CLEAN_7x13 == font:
            self._font = abspath(font)
            self._face = Face(self._font)
            self._glyph_width = 7  # width of text glyph bitmaps
            self._glyph_height = 13  # height of text glyph bitmaps
            self._glyph_xspace = 1  # horizontal space between adjacent glyphs
            self._proggyfont = True
            self._rightpadding = 1
        elif FramebufferDisplay.FONT_VGA_9x16 == font:
            self._font = abspath(font)
            self._face = Face(self._font)
            self._face.set_char_size(16 * 64)
            self._glyph_width = 9  # width of text glyph bitmaps
            self._glyph_height = 16  # height of text glyph bitmaps
            self._glyph_xspace = 0  # horizontal space between adjacent glyphs
        else:
            # default font is FramebufferDisplay.FONT_EGA_8x14
            self._font = abspath(FramebufferDisplay.FONT_EGA_8x14)
            self._face = Face(self._font)
            self._face.set_char_size(16 * 64)
            self._glyph_width = 8  # width of text glyph bitmaps
            self._glyph_height = 14  # height of text glyph bitmaps
            self._glyph_xspace = 0  # horizontal space between adjacent glyphs

    @property
    def glyph_width(self) -> int:
        """Glyph (character) width in pixels."""
        return self._glyph_width

    @property
    def glyph_height(self) -> int:
        """Glyph (character) height in pixels."""
        return self._glyph_height

    @property
    def glyph_xspace(self) -> int:
        """Horizontal space between adjacent glyphs (characters), in pixels."""
        return self._glyph_xspace

    @property
    def font(self) -> str:
        """Filesystem path of selected font file."""
        return self._font

    def line(self, x_start: int, y_start: int, x_end: int, y_end: int, value: int = None):
        """
        Draw a straight line from (x_start, y_start) to (x_end, y_end).

        :param x_start: X coordinate of the start of the line.
        :param y_start: Y coordinate of the start of the line.
        :param x_end: X coordinate of the end of the line.
        :param y_end: Y coordinate of the end of the line.
        :param value: Pixel value that sets colour and brightness, of length self._bits_per_pixel.
        """
        if value is None:
            value = self._white_value
        stepcount = max((y_end - y_start), (x_end - x_start))
        xstep = (x_end - x_start) / stepcount
        ystep = (y_end - y_start) / stepcount
        x = x_start
        y = y_start
        self.write(x, y, value)
        for s in range(stepcount):
            x += xstep
            y += ystep
            self.write(round(x), round(y), value)

    def rect(self, x_start: int, y_start: int, x_end: int, y_end: int, value: int = None, fill: bool = False):
        """
        Draw a rectangle from (x_start, y_start) to (x_end, y_end).

        :param x_start: X coordinate of the first corner of the rectangle.
        :param y_start: Y coordinate of the first corner of the rectangle.
        :param x_end: X coordinate of the opposite corner of the rectangle.
        :param y_end: Y coordinate of the opposite corner of the rectangle.
        :param value: Pixel value that sets colour and brightness, of length self._bits_per_pixel.
        :param fill: If True, draw a filled rectangle, if False draw outline only.
        """
        if value is None:
            value = self._white_value
        # draw the outline
        self.line(x_start, y_start, x_end, y_start, value)
        self.line(x_start, y_end, x_end, y_end, value)
        if not fill:
            self.line(x_start, y_start, x_start, y_end, value)
            self.line(x_end, y_start, x_end, y_end, value)
        else:
            # fill the outline
            y_step = 1 if y_end > y_start else -1
            for y in range(y_start + y_step, y_end, y_step):
                self.line(x_start, y, x_end, y, value)

    def image(self, image_path: str, x_start: int, y_start: int, x_end: int, y_end: int):
        """
        Render an image file from (x_start, y_start) to (x_end, y_end).

        The image will be resized to fit the rectangle described by the start
        and end points.
        
        The pixel format packs R, G, and B components of lengths self._red_bits,
        self._green_bits and self._blue_bits into a word of length self._bits_per_pixel.
        If the order parameter is 'RGB' the field order is: Red, Green, Blue
        (MSB to LSB). If the order parameter is 'BGR' the field order is reversed.

        There are many pixel formats used by different displays. The image()
        method has been tested with an ILI9341 TFT LCD panel and with the plot()
        method that renders the framebuffer contents on an emulated display that
        uses matplotlib. If you are using a different display, you may need to
        modify this method.

        :param image_path: File path of image file, which can be any format supported by Python Imaging Library.
        :param x_start: X coordinate of the top left corner of the image.
        :param y_start: Y coordinate of the top left corner of the rectangle.
        :param x_end: X coordinate of the bottom right corner of the rectangle.
        :param y_end: Y coordinate of the bottom right corner of the rectangle.
        """
        if self._bits_per_pixel == 1 or self._red_bits == 0 or self._green_bits == 0 or self._blue_bits == 0:
            raise NotImplementedError('Framebufferdisplay.image() method does not support monochrome displays')
        if not self._image_dependencies_imported:
            # conditional import speeds up execution time when an application does not use images
            from PIL import Image
            self._image_dependencies_imported = True
        multipliers = (pow(2, self._red_bits) - 1, pow(2, self._green_bits) - 1, pow(2, self._blue_bits) - 1)
        dividers = (255, 255, 255)
        im = Image.open(image_path)
        x_size = 1 + x_end - x_start
        y_size = 1 + y_end - y_start
        im = im.resize((x_size, y_size))
        im = im.convert('RGB')  # 24-bit RGB bitmap
        for y in range(y_size):
            for x in range(x_size):
                rgb = im.getpixel((x, y))
                r_scaled, g_scaled, b_scaled = ((rgb_component * multiplier) // divider for rgb_component, multiplier, divider in zip(rgb, multipliers, dividers))
                value = r_scaled << self._red_offset | g_scaled << self._green_offset | b_scaled << self._blue_offset
                self.write(x + x_start, y + y_start, value)

    def text(self, s: str, x_start: int = 0, y_start: int = 0, value: int = None, bg_value: int = None):
        """
        Print text to the display buffer.

        :param s: Text string to be printed.
        :param x_start: X coordinate of the top left corner of the text.
        :param y_start: Y coordinate of the top left corner of the text.
        :param value: Pixel value that sets text colour and brightness. Default is WHITE.
        :param bg_value: Pixel value that sets background colour and brightness. Default is BLACK.
        """
        if value is None:
            value = self._white_value
        if bg_value is None:
            bg_value = self._black_value
        x = x_start
        y = y_start
        for ch in s:
            if self._proggyfont:
                # proggyfonts have an error in the character indexing, so an offset must be added
                self._face.load_char(ord(ch) + 1)
            else:
                self._face.load_char(ch)
            glyphbitmap = self._face.glyph.bitmap
            # bytes from the glyph bitmaps must be bit-reversed when written to Linux framebuffer
            for row in range(glyphbitmap.rows):
                for column in range(glyphbitmap.width):
                    # proggyfonts glyph bitmaps use bits 1 to 7 only. Bit 0 is right-padding and is not rendered.
                    byteindex = (column + self._rightpadding) // 8
                    bitindex = (column + self._rightpadding) % 8
                    glyphbyte = glyphbitmap.buffer[(row * glyphbitmap.pitch) + byteindex]
                    pixelvalue = value if (1 == get_bitvalue(glyphbyte, bitindex)) else bg_value
                    self.write(x + glyphbitmap.width - column, y + row, pixelvalue)
            x += self._glyph_width + self._glyph_xspace


if __name__ == '__main__':

    # When run as a script, demonstrate the Framebuffer print function, and optionally the plot function too.
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('--text', '-t', action='store_true', help='Display monochrome text.')
    parser.add_argument('--graphics', '-g', action='store_true', help='Display monochrome graphics.')
    parser.add_argument('--plot', '-p', action='store_true',
                        help='Render simulated display using matplotlib, as well as console printing.')
    parser.add_argument('--colour', '-c', action='store_true',
                        help='Render colour JPEG image on simulated display using matplotlib.')
    args = parser.parse_args()

    if args.graphics:
        # Graphics demo.
        WHITE = 0xF
        LIGHT_GRAY = 0xC
        MID_GRAY = 0x8
        DARK_GRAY = 0x4
        display = FramebufferDisplay('grayscale')
        xmax = display.xsize - 1  # maximum X-axis coordinate value
        ymax = display.ysize - 1  # maximum Y-axis coordinate value
        display.rect(*(xmax//8, ymax//8), *(7*xmax//8, 7*ymax//8), DARK_GRAY, True)
        display.rect(*(2*xmax//8, 2*ymax//8), *(6*xmax//8, 6*ymax//8), MID_GRAY, True)
        display.rect(*(3*xmax//8, 3*ymax//8), *(5*xmax//8, 5*ymax//8), LIGHT_GRAY, True)
        display.rect(*(0, 0), *(xmax, ymax), WHITE)
        display.line(*(0, 0), *(xmax, ymax), WHITE)
        display.line(*(0, ymax), *(xmax, 0), WHITE)
        print(display)  # render on text console
        if args.plot:
            display.plot()  # render in Matplotlib window
    elif args.colour:
        # Colour JPEG image rendering demo.
        GREEN = 0x00FF00 # colour green as 24-bit RGB888 value
        displayfont = FramebufferDisplay.FONT_VGA_9x16
        display = FramebufferDisplay('colour', displayfont)
        xmax = display.xsize - 1  # maximum X-axis coordinate value
        ymax = display.ysize - 1  # maximum Y-axis coordinate value
        display.image('content/uprising-album-cover-500x495.jpg', *(60, 10), *(259, 209))
        display.text('Redemption Song [Band Version]', *(25, 215), GREEN)
        # large images printed to screen are unclear due to line wrapping. Write to a file and view with text editor.
        with open('temp.txt', 'w') as imagefile:
            print(display, file = imagefile)
        print('Open the file temp.txt in a text editor to view printed output.')
        if args.plot:
            display.plot()  # render in Matplotlib window
    else:
        # Text demo.
        # No colour/brightness value is specified when calling display.line() and display.rect() methods,
        # so the default value is used: White drawing on a black background.
        displayfont = FramebufferDisplay.FONT_EGA_8x14
        display = FramebufferDisplay('bw', displayfont)
        display.text('Hello, World!', 2, 0)
        display.text('and goodbye.', 2, display.glyph_height)
        print(display)  # render on text console
        if args.plot:
            display.plot()  # render in Matplotlib window
