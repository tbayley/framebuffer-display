#!/usr/bin/env python3
"""
Glyph bitmap monochrome rendering

This is a test script to see how characters look when rendered using a range of
different fonts.
"""

from freetype import *
import os


def bits(byteval):
    """
    Convert a byte to a list of bit values, LSB at index 0 and MSB at index 7.
    :param byteval: byte value.
    :return: list of eight bit values.
    """
    bitlist = []
    for bit in range(8):
        bitlist.insert(0, byteval & 1)
        byteval = byteval >> 1
    return bitlist


def abspath(relativepath):
    """
    Convert relative file path to absolute file path.
    :param relativepath: file path relative to this script.
    :return: absolute file path.
    """
    directory = os.path.dirname(__file__)
    absolutepath = os.path.join(directory, relativepath)
    return absolutepath


if __name__ == '__main__':
    import numpy
    import matplotlib.pyplot as plt

    style = 0  # Change 'style' to change the font that is rendered

    if 1 == style:
        # Although this is a monospace font, TTF format renders some characters
        # smaller than the 5x7 pixel bounding box, which makes typesetting
        # difficult! It is easier to use bitmap font file formats (PCF or OTB)
        # so that the glyph size is always equal to the bounding box size.
        font = abspath('fonts/repetition/repetition_5x7.ttf')  # monospace: 5x7 pixels
        face = Face(font)
        face.set_char_size(10 * 64)
        face.load_char('S', FT_LOAD_RENDER | FT_LOAD_TARGET_MONO)
    elif 2 == style:
        font = abspath('fonts/proggyfonts/ProggySmall_7x10.pcf.gz')  # monospace: 7x10 pixels
        face = Face(font)
        face.load_char(ord('W') + 1)
    elif 3 == style:
        font = abspath('fonts/proggyfonts/ProggyClean_7x13.pcf.gz')  # monospace: 7x13 pixels
        face = Face(font)
        face.load_char(ord('W') + 1)
    elif 4 == style:
        font = abspath('fonts/oldschool/Bm437_IBM_EGA_8x14.otb')  # monospace: 8x14 pixels
        face = Face(font)
        face.set_char_size(16 * 64)
        face.load_char('W')
    else:  # 0 == style
        # VGA 9x16 'plus' font can render unicode characters such as μ, ö and é
        font = abspath('fonts/oldschool/BmPlus_IBM_VGA_9x16.otb')  # monospace: 9x16 pixels
        face = Face(font)
        face.set_char_size(16 * 64)
        face.load_char('W')

    bitmap = face.glyph.bitmap
    width = bitmap.width
    rows = bitmap.rows
    pitch = bitmap.pitch
    print(f'width: {width}, rows: {rows}, pitch: {pitch}', flush=True)

    data = []
    for i in range(bitmap.rows):
        row = []
        for j in range(bitmap.pitch):
            row.extend(bits(bitmap.buffer[i*bitmap.pitch+j]))
        data.extend(row[:bitmap.width])
    Z = numpy.array(data).reshape(bitmap.rows, bitmap.width)
    plt.imshow(Z, interpolation='nearest', cmap=plt.cm.gray, origin='upper')
    plt.show()
