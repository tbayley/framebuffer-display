# FRAMEBUFFER DISPLAY UTILITIES #

This repository contains Python utilities to write text and graphics to OLED and
LCD displays using Linux kernel drivers. Those drivers implement Linux
framebuffer devices. The utilities are intended for use on embedded Linux
systems such as Raspberry Pi.

Two example applications are implemented:

- _displayaddr.py_ writes the system's IP address to an SSD1306 128x32 pixel
  monochrome OLED display. Instructions are also provided to run this script as
  a Linux service that starts at boot-time.

- _colourdemo.py_ displays album cover art and track title on an ILI9341 320x240
  pixel colour TFT LCD display, similar to a media player application.


## How do I get set up? ##

The following instructions assume that you are connecting either an SSD1306
128x32 pixel monochrome OLED display, or an ILI9341 320x240 pixel colour TFT LCD
display to a Raspberry Pi computer. If you are using a different Linux computer
or display module, details may be different.

Clone this git repository. The file paths and user that are specified in the
_systemd_ service definition file _displayaddr.service_ assume that the username
is _pi_ (the default username for a Raspberry Pi), and that the repository is
located in the user's home directory at _/home/pi/framebuffer-display_. If your
username is not _pi_, or if you have cloned the repository in a different
location, you will have to edit _displayaddr.service_ accordingly.


### Install software packages ###

First install the Linux packages and shared libraries that will are needed by
Python.

```bash
sudo apt install build-essential    # required to build Python packages
sudo apt install libatlas-base-dev  # required for numpy on Raspberry Pi
sudo apt install bluetooth bluez libbluetooth3 libbluetooth-dev bluez-tools
```

Then run the following command from the repository's root directory to install
or upgrade the required Python packages.

```bash
pip3 install --upgrade -r requirements.txt
```

If this command reports that PyBluez installation fails with
_'error in PyBluez setup command: use_2to3 is invalid'_, install PyBluez with
`sudo apt install python3-bluez` 

Then install _requirements.txt_ again.


### Connect and configure the display module ###

Connect and configure your display module as described on one of the following pages:

- **SSD1306 128x32 pixel monochrome OLED display** : [SSD1306-OLED-128x32px.md](./SSD1306-OLED-128x32px.md)

- **ILI9341 320x240 pixel colour TFT LCD display** : [ILI9341-TFT-LCD-320x240px.md](./ILI9341-TFT-LCD-320x240px.md)


## Applications ##

### Display IP address on SSD1306 128x32 pixel monochrome OLED ###

#### Run the IP address display script manually ####

Run the following command to write the system's hostname and IP address to the
display:

```bash
python3 displayaddr.py
```

The script runs in an infinite loop, polling periodically to detect changes to
the IP address, which it writes to the display. Press <Ctrl> + C to terminate
the script.


#### Run the IP address display script automatically at boot-time ####

To automatically write the IP address to the OLED display at boot-time, we use
a custom _displayaddr_ service. The service is defined in the file
_displayaddr.service_. To avoid having to install the required Python packages
globally (i.e. using _sudo pip3 install ..._), this service is run as user _pi_
(the default user account on Raspberry Pi) instead of as _root_. If your user
name is not _pi_, or you have not cloned this repository in _/home/pi/framebuffer-display_,
then you must edit the following lines in _displayaddr.service_:
```bash
WorkingDirectory=/home/pi/framebuffer-display/
User=pi
```

When editing is complete, copy _displayaddr.service_ to _/etc/systemd/system/_
as root:
```bash
sudo cp displayaddr.service /etc/systemd/system/
```

You can then start _displayaddr.service_ using the command:
```bash
sudo systemctl start displayaddr.service
```

And stop the service with the command:
```bash
sudo systemctl stop displayaddr.service
```

To have the service start automatically on reboot, use the commands:
```bash
sudo systemctl daemon-reload
sudo systemctl enable displayaddr.service --now
```


### Display album cover art on ILI9341 320x240 pixel colour TFT LCD ###

Run the following command to display a mock-up of a media player screen,
including album cover artwork and track name, on an ILI9341 320x240 pixel colour
TFT LCD.

```bash
python3 colourdemo.py
```


### framebufferdisplay.py demo script ###

The framebufferdisplay.py module implements the FramebufferDisplay Python class.
However, if run as a script, it performs one of three tests. The test each use a
different mock framebuffer display that is emulated by a Matplotlib image window
(on systems with a monitor attached) and / or by printed text (if using SSH
connection to a headless embedded host). The emulated displays let you evaluate
the FramebufferDisplay class, or develop applications that use it, even on a
host that does not have a real framebuffer display attached (even a Windows
host!).

The three test application are run by invoking the script
with different command line parameters:

- `python3 framebufferdisplay.py --plot`:
  Text display demo using emulated 128 x 32 pixel monochrome black and white display.

- `python3 framebufferdisplay.py --graphics --plot`:
  Graphics display demo using emulated 128 x 64 pixel 4-bit greyscale display.

- `python3 framebufferdisplay.py --colour --plot`:
  JPEG image display demo using emulated 320 x 240 pixel 24-bit colour display.

If you are using a host that does not have a monitor attached, and therefore
cannot use Matplotlib, omit the `--plot` command line option. Then the
framebuffer image will be rendered in a simplified form by printing text that
can be viewed via a remote SSH connection.


### framebufferscreeninfo demo script ###

The framebufferscreeninfo.py module implements the FramebufferScreenInfo Python
class. However, if run as a script, it uses that class to access the struct
_fb\_var\_screeninfo_ for the selected frame buffer device and print it to the
console. To print configuration data fro screenbuffer device _/dev/fb1_, for
example, you would run the script like this:

```bash
python3 framebufferscreeninfo.py /dev/fb1
```


## Copyright ###

Copyright (C) 2023 Antony Bayley.  All Rights Reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
