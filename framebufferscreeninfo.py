#!/usr/bin/env python3

# Python class to access framebuffer screen info struct on Linux.
#
# This class accesses the struct fb_var_screeninfo, which stores device
# independent changeable information about a frame buffer device, its current
# format and video mode, as well as other miscellaneous parameters.
#
# Version 1.0
# Copyright (C) 2023 Antony Bayley.  All Rights Reserved.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import struct

try:
    import fcntl
except ImportError:
    # mock fcntl module enables limited testing on Windows.
    fcntl = None


class FramebufferBitfield:
    """
    Linux framebuffer bitfield for storage of red, green, blue and transparent field data.

    If framebuffer uses truecolour visual mode, then all fields are set. Otherwise only length field is valid.

    See documentation for 'struct fb_bitfield' in Linux Frame Buffer Device API
    https://www.kernel.org/doc/Documentation/fb/api.txt
    """
    
    RAW_DATA_LEN = 12  # Raw data length, in bytes: three uint32_t fields.

    def __init__(self, raw_data: bytearray):
        """
        FramebufferBitfield initialiser.

        :param raw_data: C struct containing offset, length and msb_right fields
                         as uint32_t values.
        """
        val = struct.unpack('=III', raw_data[0: self.RAW_DATA_LEN])
        self._offset = val[0]
        self._length = val[1]
        self._msb_right = val[2]

    def __repr__(self) -> str:
        """
        Render FramebufferBitfield object for viewing on a text console.

        If fb is an object of type FramebufferBitfield, it is rendered to the text console by executing the command:

            print(fb)

        :return: String representation of the FramebufferBitfield object.
        """
        val = f'FramebufferBitfield {{ offset: {self._offset}, length: {self._length}, msb_right: {self.msb_right} }}'
        return val

    @property
    def offset(self) -> int:
        """Offset to beginning of bitfield."""
        return self._offset

    @property
    def length(self) -> int:
        """Length of bitfield, in bits."""
        return self._length

    @property
    def msb_right(self) -> bool:
        """
        Position of most significant bit.

        False: MSB is left.
        True: MSB is right.
        """
        return 0 != self._offset


class FramebufferScreenInfo:
    """
    Screen information for Linux framebuffer.

    Data are obtained using FBIOGET_VSCREENINFO ioctl. For Linux Frame Buffer
    Device API, see https://www.kernel.org/doc/Documentation/fb/api.txt
    """
    _FBIOGET_VSCREENINFO = 0x4600

    # Byte offsets of fields in the C struct 'fb_var_screeninfo'
    _VSCREENINFO_OFFSET_XRES = 0                # x axis visible resolution
    _VSCREENINFO_OFFSET_YRES = 4                # y axis visible resolution
    _VSCREENINFO_OFFSET_XRES_VIRTUAL = 8        # x axis virtual resolution
    _VSCREENINFO_OFFSET_YRES_VIRTUAL = 12       # y axis virtual resolution
    _VSCREENINFO_OFFSET_XOFFSET = 16            # x axis offset from virtual to visible resolution
    _VSCREENINFO_OFFSET_YOFFSET = 20            # y axis offset from virtual to visible resolution
    _VSCREENINFO_OFFSET_BITS_PER_PIXEL = 24     # bits per pixel
    _VSCREENINFO_OFFSET_GRAYSCALE = 28          # 0 = colour, 1 = grayscale, >1 = FOURCC
    _VSCREENINFO_OFFSET_RED = 32                # red colour component fb_bitfield struct
    _VSCREENINFO_OFFSET_GREEN = 44              # green colour component fb_bitfield struct
    _VSCREENINFO_OFFSET_BLUE = 56               # blue colour component fb_bitfield struct
    _VSCREENINFO_OFFSET_TRANSP = 68             # transparency component fb_bitfield struct
    _VSCREENINFO_OFFSET_NONSTD = 80             # non-zero value denotes a non-standard pixel format
    _VSCREENINFO_OFFSET_ACTIVATE = 84           # see FB_ACTIVATE_
    _VSCREENINFO_OFFSET_HEIGHT = 88             # height of picture in millimetres
    _VSCREENINFO_OFFSET_WIDTH = 92              # width of picture in millimetres
    _VSCREENINFO_OFFSET_ACCEL_FLAGS = 96        # (OBSOLETE) see fb_info.flags
    _VSCREENINFO_OFFSET_PIXCLOCK = 100          # pixel clock period in picoseconds
    _VSCREENINFO_OFFSET_LEFT_MARGIN = 104       # time from sync to picture, in pixclocks
    _VSCREENINFO_OFFSET_RIGHT_MARGIN = 108      # time from picture to sync, in pixclocks
    _VSCREENINFO_OFFSET_UPPER_MARGIN = 112      # time from sync to picture, in pixclocks
    _VSCREENINFO_OFFSET_LOWER_MARGIN = 116      # time from picture to sync, in pixclocks
    _VSCREENINFO_OFFSET_HSYNC_LEN = 120         # length of horizontal sync, in pixclocks
    _VSCREENINFO_OFFSET_VSYNC_LEN = 124         # length of vertical sync, in pixclocks
    _VSCREENINFO_OFFSET_SYNC = 128              # see FB_SYNC_
    _VSCREENINFO_OFFSET_VMODE = 132             # see FB_VMODE_
    _VSCREENINFO_OFFSET_ROTATE = 136            # angle of rotation counter clockwise
    _VSCREENINFO_OFFSET_COLORSPACE = 140        # colorspace for FOURCC-based modes

    _VSCREENINFO_LEN = 148  # Length of C struct 'fb_var_screeninfo'

    _UINT32_LEN = 4  # Length of uint32_t struct member, in bytes

    def __init__(self, framebuffer: str):
        """
        FramebufferScreenInfo initialiser, obtains data using ioctl.

        :param framebuffer: Filesystem path of Linux framebuffer.
        """
        self._framebuffer = framebuffer
        self._screeninfo = bytearray(self._VSCREENINFO_LEN)  # store the entire screeninfo struct
        with open(self._framebuffer, 'r') as fb:
            fcntl.ioctl(fb, self._FBIOGET_VSCREENINFO, self._screeninfo)
    
    @property
    def framebuffer(self) -> str:
        """Filesystem path of Linux framebuffer."""
        return self._framebuffer

    @property
    def xres(self) -> int:
        """X-axis (width) visible resolution."""
        val = struct.unpack('=I', self._screeninfo[self._VSCREENINFO_OFFSET_XRES:
                                                   self._VSCREENINFO_OFFSET_XRES + self._UINT32_LEN])
        return val[0]

    @property
    def yres(self) -> int:
        """Y-axis (height) visible resolution."""
        val = struct.unpack('=I', self._screeninfo[self._VSCREENINFO_OFFSET_YRES:
                                                   self._VSCREENINFO_OFFSET_YRES + self._UINT32_LEN])
        return val[0]

    @property
    def xres_virtual(self) -> int:
        """X-axis (width) virtual resolution."""
        val = struct.unpack('=I', self._screeninfo[self._VSCREENINFO_OFFSET_XRES_VIRTUAL:
                                                   self._VSCREENINFO_OFFSET_XRES_VIRTUAL + self._UINT32_LEN])
        return val[0]

    @property
    def yres_virtual(self) -> int:
        """Y-axis (height) virtual resolution."""
        val = struct.unpack('=I', self._screeninfo[self._VSCREENINFO_OFFSET_YRES_VIRTUAL:
                                                   self._VSCREENINFO_OFFSET_YRES_VIRTUAL + self._UINT32_LEN])
        return val[0]

    @property
    def xoffset(self) -> int:
        """X-axis (width) offset from virtual to visible resolution."""
        val = struct.unpack('=I', self._screeninfo[self._VSCREENINFO_OFFSET_XOFFSET:
                                                   self._VSCREENINFO_OFFSET_XOFFSET + self._UINT32_LEN])
        return val[0]

    @property
    def yoffset(self) -> int:
        """Y-axis (height) offset from virtual to visible resolution."""
        val = struct.unpack('=I', self._screeninfo[self._VSCREENINFO_OFFSET_YOFFSET:
                                                   self._VSCREENINFO_OFFSET_YOFFSET + self._UINT32_LEN])
        return val[0]

    @property
    def bits_per_pixel(self) -> int:
        """Bits per pixel."""
        val = struct.unpack('=I', self._screeninfo[self._VSCREENINFO_OFFSET_BITS_PER_PIXEL:
                                                   self._VSCREENINFO_OFFSET_BITS_PER_PIXEL + self._UINT32_LEN])
        return val[0]

    @property
    def grayscale(self) -> int:
        """
        Grayscale / colour mode of the display.
        
        0: Colour
        1: Grayscale
        > 1: FOURCC 
        """
        val = struct.unpack('=I', self._screeninfo[self._VSCREENINFO_OFFSET_GRAYSCALE:
                                                   self._VSCREENINFO_OFFSET_GRAYSCALE + self._UINT32_LEN])
        return val[0]

    @property
    def red(self) -> FramebufferBitfield:
        """
        Red framebuffer bitfield.

        If framebuffer uses truecolour visual mode, then all fields are set. Otherwise only length field is valid.
        """
        val = FramebufferBitfield(self._screeninfo[self._VSCREENINFO_OFFSET_RED:
                                                   self._VSCREENINFO_OFFSET_RED + FramebufferBitfield.RAW_DATA_LEN])
        return val

    @property
    def green(self) -> FramebufferBitfield:
        """
        Green framebuffer bitfield.

        If framebuffer uses truecolour visual mode, then all fields are set. Otherwise only length field is valid.
        """
        val = FramebufferBitfield(self._screeninfo[self._VSCREENINFO_OFFSET_GREEN:
                                                   self._VSCREENINFO_OFFSET_GREEN + FramebufferBitfield.RAW_DATA_LEN])
        return val

    @property
    def blue(self) -> FramebufferBitfield:
        """
        Blue framebuffer bitfield.

        If framebuffer uses truecolour visual mode, then all fields are set. Otherwise only length field is valid.
        """
        val = FramebufferBitfield(self._screeninfo[self._VSCREENINFO_OFFSET_BLUE:
                                                   self._VSCREENINFO_OFFSET_BLUE + FramebufferBitfield.RAW_DATA_LEN])
        return val

    @property
    def transp(self) -> FramebufferBitfield:
        """
        Transparency framebuffer bitfield.

        If framebuffer uses truecolour visual mode, then all fields are set. Otherwise only length field is valid.
        """
        val = FramebufferBitfield(self._screeninfo[self._VSCREENINFO_OFFSET_TRANSP:
                                                   self._VSCREENINFO_OFFSET_TRANSP + FramebufferBitfield.RAW_DATA_LEN])
        return val

    @property
    def nonstd(self) -> bool:
        """True denotes a non-standard pixel format."""
        val = struct.unpack('=I', self._screeninfo[self._VSCREENINFO_OFFSET_NONSTD:
                                                   self._VSCREENINFO_OFFSET_NONSTD + self._UINT32_LEN])
        return 0 != val[0]

    @property
    def activate(self) -> int:
        """Value of FB_ACTIVATE_ field."""
        val = struct.unpack('=I', self._screeninfo[self._VSCREENINFO_OFFSET_ACTIVATE:
                                                   self._VSCREENINFO_OFFSET_ACTIVATE + self._UINT32_LEN])
        return val[0]

    @property
    def height(self) -> int:
        """Height of picture in millimetres."""
        val = struct.unpack('=I', self._screeninfo[self._VSCREENINFO_OFFSET_HEIGHT:
                                                   self._VSCREENINFO_OFFSET_HEIGHT + self._UINT32_LEN])
        return val[0]

    @property
    def width(self) -> int:
        """Width of picture in millimetres."""
        val = struct.unpack('=I', self._screeninfo[self._VSCREENINFO_OFFSET_WIDTH:
                                                   self._VSCREENINFO_OFFSET_WIDTH + self._UINT32_LEN])
        return val[0]

    @property
    def accel_flags(self) -> int:
        """Value of fb_info.flags field (OBSOLETE)."""
        val = struct.unpack('=I', self._screeninfo[self._VSCREENINFO_OFFSET_ACCEL_FLAGS:
                                                   self._VSCREENINFO_OFFSET_ACCEL_FLAGS + self._UINT32_LEN])
        return val[0]

    @property
    def pixclock(self) -> int:
        """Pixel clock period in picoseconds."""
        val = struct.unpack('=I', self._screeninfo[self._VSCREENINFO_OFFSET_PIXCLOCK:
                                                   self._VSCREENINFO_OFFSET_PIXCLOCK + self._UINT32_LEN])
        return val[0]

    @property
    def left_margin(self) -> int:
        """Time from sync to picture, in pixclocks."""
        val = struct.unpack('=I', self._screeninfo[self._VSCREENINFO_OFFSET_LEFT_MARGIN:
                                                   self._VSCREENINFO_OFFSET_LEFT_MARGIN + self._UINT32_LEN])
        return val[0]

    @property
    def right_margin(self) -> int:
        """Time from picture to sync, in pixclocks."""
        val = struct.unpack('=I', self._screeninfo[self._VSCREENINFO_OFFSET_RIGHT_MARGIN:
                                                   self._VSCREENINFO_OFFSET_RIGHT_MARGIN + self._UINT32_LEN])
        return val[0]

    @property
    def upper_margin(self) -> int:
        """Time from sync to picture, in pixclocks."""
        val = struct.unpack('=I', self._screeninfo[self._VSCREENINFO_OFFSET_UPPER_MARGIN:
                                                   self._VSCREENINFO_OFFSET_UPPER_MARGIN + self._UINT32_LEN])
        return val[0]

    @property
    def lower_margin(self) -> int:
        """Time from picture to sync, in pixclocks."""
        val = struct.unpack('=I', self._screeninfo[self._VSCREENINFO_OFFSET_LOWER_MARGIN:
                                                   self._VSCREENINFO_OFFSET_LOWER_MARGIN + self._UINT32_LEN])
        return val[0]

    @property
    def hsync_len(self) -> int:
        """Length of horizontal sync, in pixclocks."""
        val = struct.unpack('=I', self._screeninfo[self._VSCREENINFO_OFFSET_HSYNC_LEN:
                                                   self._VSCREENINFO_OFFSET_HSYNC_LEN + self._UINT32_LEN])
        return val[0]

    @property
    def vsync_len(self) -> int:
        """Length of vertical sync, in pixclocks."""
        val = struct.unpack('=I', self._screeninfo[self._VSCREENINFO_OFFSET_VSYNC_LEN:
                                                   self._VSCREENINFO_OFFSET_VSYNC_LEN + self._UINT32_LEN])
        return val[0]

    @property
    def sync(self) -> int:
        """Value of FB_SYNC_ field."""
        val = struct.unpack('=I', self._screeninfo[self._VSCREENINFO_OFFSET_SYNC:
                                                   self._VSCREENINFO_OFFSET_SYNC + self._UINT32_LEN])
        return val[0]

    @property
    def vmode(self) -> int:
        """Value of FB_VMODE_ field."""
        val = struct.unpack('=I', self._screeninfo[self._VSCREENINFO_OFFSET_VMODE:
                                                   self._VSCREENINFO_OFFSET_VMODE + self._UINT32_LEN])
        return val[0]

    @property
    def rotate(self) -> int:
        """Angle of rotation counter clockwise."""
        val = struct.unpack('=I', self._screeninfo[self._VSCREENINFO_OFFSET_ROTATE:
                                                   self._VSCREENINFO_OFFSET_ROTATE + self._UINT32_LEN])
        return val[0]

    @property
    def colorspace(self) -> int:
        """Colorspace for FOURCC-based modes."""
        val = struct.unpack('=I', self._screeninfo[self._VSCREENINFO_OFFSET_COLORSPACE:
                                                   self._VSCREENINFO_OFFSET_COLORSPACE + self._UINT32_LEN])
        return val[0]


if __name__ == '__main__':

    import sys

    if len(sys.argv) > 1:
        framebuffer = sys.argv[1]
    else:
        framebuffer = '/dev/fb1'

    try:
        info = FramebufferScreenInfo(framebuffer)
    except FileNotFoundError:
        print(f'Framebuffer device {framebuffer} was not found.')
        sys.exit(1)

    print(f'framebuffer = {info.framebuffer}')
    print(f'xres = {info.xres}')
    print(f'yres = {info.yres}')
    print(f'xres_virtual = {info.xres_virtual}')
    print(f'yres_virtual = {info.yres_virtual}')
    print(f'xoffset = {info.xoffset}')
    print(f'yoffset = {info.yoffset}')
    print(f'bits_per_pixel = {info.bits_per_pixel}')
    print(f'grayscale = {info.grayscale}')
    print(f'red = {info.red}')
    print(f'green = {info.green}')
    print(f'blue = {info.blue}')
    print(f'transp = {info.transp}')
    print(f'nonstd = {info.nonstd}')
    print(f'activate = {info.activate}')
    print(f'height = {info.height}')
    print(f'width = {info.width}')
    print(f'accel_flags = {info.accel_flags}')
    print(f'pixclock = {info.pixclock}')
    print(f'left_margin = {info.left_margin}')
    print(f'right_margin = {info.right_margin}')
    print(f'upper_margin = {info.upper_margin}')
    print(f'lower_margin = {info.lower_margin}')
    print(f'hsync_len = {info.hsync_len}')
    print(f'vsync_len = {info.vsync_len}')
    print(f'sync = {info.sync}')
    print(f'vmode = {info.vmode}')
    print(f'rotate = {info.rotate}')
    print(f'colorspace = {info.colorspace}')
