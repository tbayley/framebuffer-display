# SSD1306 128x32 pixel monochrome OLED display

![Photo of OLED](./content/SSD1306-oled-display-128x32.jpg "SSD1306 monochrome OLED display (128x32 pixels)")

Get a 128x32 pixel SSD1306 OLED display, from suppliers such as:

* [AliExpress](https://www.aliexpress.com/item/32672229793.html)
* [eBay](https://www.ebay.co.uk/itm/272626889909)
* [Amazon](https://www.amazon.co.uk/AITRIP-Display-SSD1306-3-3V-5V-Arduino/dp/B08H1LB6JZ/ref=sr_1_7)


## Electrical connections

This display module has an I2C interface. The Raspberry Pi's 40-pin GPIO header
has on-board pull-up resistors for the I2C clock and data lines, so just four
jumper wires are needed to connect the OLED display module.

| Raspberry Pi GPIO Pin Number   | Raspberry Pi GPIO Pin Name | SSD1306 OLED Pin Name |
| :----------------------------: | :------------------------: | :-------------------: |
|               1                |           3.3V             |         VCC           |
|               3                |           I2C_SDA          |         SDA           |
|               5                |           I2C_SCL          |         SCK           |
|               6                |           GND              |         GND           |


## Configure Device Tree Overlays (Linux display driver) ###

This software uses a Linux kernel driver for the display. The driver is loaded
at boot-time by configuring device-tree overlays. To load the device tree
overlays for a 128x32 pixel SSD1306 OLED, and configure the Raspberry Pi's I2C
interface, it is necessary to edit the file _/boot/config.txt_ and include the
_ssd1306_ device tree overlay. Framebuffer _/dev/fb0_ is used for the main
system display on the Raspberry Pi's HDMI connector. Framebuffer _/dev/fb1_ is
used for the LCD display module.

By default, if the HDMI cable is not connected to a monitor, the Linux system
uses framebuffer _/dev/fb0_ as the data source for the first display that is
detected. It makes no sense to use a small LCD as the main system display, so
we must override this behaviour. At the time of writing (December 2022), the
current version of Raspberry Pi OS is _"Bullseye"_. The default video driver for
_"Bullseye"_ is DRM VC4 V3D, but _"Buster"_ and earlier OS versions use a fake
KMS video driver. These two video drivers have different ways to to force an
association between the HDMI video interface and default framebuffer _/device/fb0_
when the HDMI cable is disconnected. Therefore, different device tree overlays
are required for _Bullseye_ and earlier Raspberry Pi OS versions.


### Raspberry Pi OS version Bullseye or later

If you are using Raspberry Pi OS _"Bullseye"_ or later, the default video driver
is DRM VC4 V3D, which does not support the option _hdmi_force_hotplug=1_ to
force the HDMI interface to be associated with the default framebuffer _/dev/fb0_.
The solution is to revert to the old fake kms driver, and set _max\_framebuffers=3_.

```bash
# Configure SSD1306 128x36 pixel OLED display using I2C interface at 400 kHz
dtoverlay=ssd1306,inverted,sequential,width=128,height=32
dtparam=i2c_arm=on
dtparam=i2c_arm_baudrate=400000

# Configure  HDMI display to use /dev/fb0, even if HDMI cable is not plugged
# in, so that the OLED display always uses /dev/fb1 and does not become the
# main system console.
hdmi_force_hotplug=1

# Disable the default DRM VC4 V3D video driver
#dtoverlay=vc4-kms-v3d

# Use the old fake kms video driver that supports hdmi_force_hotplug=1 option
dtoverlay=vc4-fkms-v3d
max_framebuffers=3
```


### Raspberry Pi OS version Buster or earlier

```bash
# Configure SSD1306 128x36 pixel display using I2C interface at 400 kHz
dtoverlay=ssd1306,inverted,sequential,width=128,height=32
dtparam=i2c_arm=on
dtparam=i2c_arm_baudrate=400000

# Configure  HDMI display to use /dev/fb0, even if HDMI cable is not plugged
# in, so that the OLED display always uses /dev/fb1 and does not become the
# main system console.
hdmi_force_hotplug=1
```


## Verifying that display module is succesfully connected and configured

After rebooting the Raspberry Pi, use the _lsmod_ command to verify that the
relevant kernel modules have been loaded:

```bash
pi@raspi1:~ $ lsmod | grep ssd
ssd1307fb              20480  0
syscopyarea            16384  1 ssd1307fb
sysfillrect            16384  1 ssd1307fb
sysimgblt              16384  1 ssd1307fb
fb_sys_fops            16384  1 ssd1307fb
backlight              20480  1 ssd1307fb
```

When the ssd1306 device tree overlay is included, the Raspberry Pi implements
a second Linux framebuffer device _/dev/fb1_. To check that the display is
wired up and configured correctly, you can test it from the command line with
the following _bash_ commands:

```bash
cat /dev/urandom > /dev/fb1    # write random data to the pixels
cat /dev/zero > /dev/fb1       # clear the display (all pixels off)
cat /dev/zero | tr "\000" "\377" > /dev/fb1    # set all pixels on
```

If you want to find out more about Linux kernel modules, perhaps to adapt the
software in this repository to work with a different framebuffer-based display
module, the following references may be useful. All available Raspberry Pi kernel
modules, and a brief description of how they are used, are listed here:

* https://github.com/raspberrypi/firmware/blob/master/boot/overlays/README

The following document explains in greater detail how to use device trees on Raspberry Pi:

* https://www.raspberrypi.org/documentation/configuration/device-tree.md#part3
